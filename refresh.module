<?php

/**
 * @file
 * Provides a configurable meta refresh when viewing individual nodes.
 */

define('REFRESH_DISABLED', 0);
define('REFRESH_ENABLED', 1);

/**
 * Settings form.
 */
function refresh_admin() {
  $form = array();
  $form['refresh_default_value'] = array(
    '#type' => 'textfield',
    '#attributes' => array(
      'data-type' => 'number',
    ),
    '#title' => t('Default value for auto refresh'),
    '#default_value' => variable_get('refresh_default_value', 0),
    '#size' => 4,
    '#maxlength' => 4,
    '#description' => t("If you want all nodes to refresh by default, provide a number of seconds. Enter <em>0</em> to disable. This value will be overriden on nodes with custom value entered."),
    '#required' => TRUE,
  );
  $form['refresh_default_value_all_pages'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable for non-node pages also'),
    '#default_value' => variable_get('refresh_default_value_all_pages', FALSE),
    '#description' => t("Check this box if you also want all pages, even non-nodes, to be auto-refreshed."),
  );

  return system_settings_form($form);
}

/**
 * Settings form validation callback.
 */
function refresh_admin_validate($form, &$form_state) {
  $maxdisp = $form_state['values']['refresh_default_value'];
  if (!is_numeric($maxdisp)) {
    form_set_error('refresh_default_value', t('You must enter an integer for the default refresh value.'));
  }
  elseif ($maxdisp < 0) {
    form_set_error('refresh_default_value', t('Number of seconds should be positive.'));
  }
}

/**
 * Implements hook_menu().
 */
function refresh_menu() {
  $items = array();
  $items['admin/config/user-interface/refresh'] = array(
    'title' => 'Refresh module settings',
    'description' => 'Optionally set default value for refresh on every node.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('refresh_admin'),
    'access arguments' => array('administer refreshes'),
    'type' => MENU_NORMAL_ITEM,
  );

  return $items;
}


/**
 * Implements hook_permission().
 *
 * Creates two new permissions: one to create refreshes and one the change
 * the global settings of the module.
 */
function refresh_permission() {
  return array(
    'create refreshes' => array(
      'title' => t('Create refresh settings'),
      'description' => t('Create refresh settings for nodes'),
    ),
    'administer refreshes' => array(
      'title' => t('Change refresh settings'),
      'description' => t('Change global refresh settings'),
    ),
  );
}

/**
 * Implements hook_form_BASE_FORM_ID_alter().
 *
 * Adds a refresh group and numeric edit field to the node editing form.
 */
function refresh_form_node_form_alter(&$form, $form_state) {
  if (refresh_get_setting($form['#node']->type) == REFRESH_DISABLED) {
    return;
  }

  $refresh = '';
  $destination = '';

  if (isset($form['#node']->refresh)) {
    $refresh = $form['#node']->refresh;
  }

  if (isset($form['#node']->destination)) {
    $destination = $form['#node']->destination;
  }

  $form['refresh'] = array(
    '#type' => 'fieldset',
    '#title' => t('Refresh settings'),
    '#collapsible' => TRUE,
    '#collapsed' => empty($refresh),
    '#access' => user_access('create refreshes'),
    '#weight' => 30,
    '#group' => 'additional_settings',
    '#attributes' => array(
      'class' => array('refresh-form'),
    ),
    '#attached' => array(
      'js' => array(drupal_get_path('module', 'refresh') . '/refresh.js'),
    ),
  );
  $form['refresh']['refresh'] = array(
    '#title' => t('Duration in seconds'),
    '#type' => 'textfield',
    '#default_value' => $refresh,
    '#maxlength' => 4,
    '#size' => 4,
    '#description' => t('Optionally provide a number of seconds after which the node page refreshes.'),
  );

  $form['refresh']['destination'] = array(
    '#title' => t('Destination URL'),
    '#type' => 'textfield',
    '#default_value' => $destination,
    '#description' => t('Optionally provide a full url to a destination, if no url is given it will refresh to the current node.'),
  );
}

/**
 * Implements hook_node_view().
 *
 * Add a refresh tag when viewing the node by itself, not as a teaser.
 */
function refresh_node_view($node, $view_mode) {
  // Check if refresh is enabled for the content type.
  if (refresh_get_setting($node->type) == REFRESH_DISABLED) {
    return;
  }

  $custom_node_refresh = $node->refresh;
  $global_refresh_value = variable_get('refresh_default_value', 0);
  $use_global_refresh = variable_get('refresh_default_value_all_pages', FALSE);

  // TODO: add check for editing nodes using overlay
  if (($view_mode == 'full') && node_is_page($node)) {
    if ($custom_node_refresh === FALSE && $global_refresh_value) {
      if ($use_global_refresh) {
        $content = check_plain($global_refresh_value);
        if (!empty($node->destination)) {
          $content .= '; URL=' . $node->destination;
        }
        $element = array(
          '#tag' => 'meta',
          '#attributes' => array(
            'http-equiv' => 'refresh',
            'content' => $content,
          ),
        );
      }
    }
    elseif ($custom_node_refresh) {
      $content = check_plain($custom_node_refresh);
      if (!empty($node->destination)) {
        $content .= '; URL=' . $node->destination;
      }
      $element = array(
        '#tag' => 'meta',
        '#attributes' => array(
          'http-equiv' => 'refresh',
          'content' => $content,
        ),
      );
    }
    if (isset($element)) {
      drupal_add_html_head($element, 'refresh');
    }
  }
}

/**
 * Implements hook_node_load().
 */
function refresh_node_load($nodes, $types) {
  foreach ($nodes as $nid => $node) {
    $query = db_select('node_refresh', 'nr');
    $query->fields('nr', array('seconds', 'destination',));
    $query->condition('nid', $node->nid);
    $query->condition('vid', $node->vid);
    $result = $query->execute()->fetchAssoc();

    $nodes[$nid]->refresh = $result['seconds'];
    $nodes[$nid]->refresh_default = variable_get('refresh_default_value', 0) ? variable_get('refresh_default_value', 0) : FALSE;
    $nodes[$nid]->destination = $result['destination'];
  }
}

/**
 * Implements hook_preprocess_html().
 */
function refresh_preprocess_html(&$variables) {
  $menu = menu_get_object();
  // If we are in a node, return. Hook_node_load will take over.
  if ($menu && isset($menu->nid)) {
    return;
  }

  // If we are on admin pages, return.
  if (path_is_admin(current_path())) {
    return;
  }

  $global_refresh_all_pages = variable_get('refresh_default_value_all_pages', FALSE);
  $global_refresh_value = variable_get('refresh_default_value', 0);
  if (!$global_refresh_value || !$global_refresh_all_pages) {
    return;
  }

  $element = array(
    '#tag' => 'meta',
    '#attributes' => array(
      'http-equiv' => 'refresh',
      'content' => check_plain($global_refresh_value),
    ),
  );

  drupal_add_html_head($element, 'refresh');
}

/**
 * Implements hook_node_delete().
 */
function refresh_node_delete($node) {
  if (user_access('create refreshes') || user_access('administer refreshes')) {
    $query = db_delete('node_refresh');
    $query->condition('nid', $node->nid);
    $query->condition('vid', $node->vid);
    $query->execute();
  }
}

/**
 * Implements hook_node_validate().
 */
function refresh_node_validate($node, $form, &$form_state) {
  if (user_access('create refreshes') || user_access('administer refreshes')) {
    $refresh = trim($node->refresh);
    if ((drupal_strlen($refresh) > 0) && (!ctype_digit($refresh) || ((int) $refresh < 0))) {
      form_set_error('refresh', t('The refresh number is invalid. Please a non-negative number of seconds.'));
    }
  }
}

/**
 * Implements hook_node_insert().
 */
function refresh_node_insert($node) {
  if (user_access('create refreshes') || user_access('administer refreshes')) {
    $destination = '';
    if (isset($node->destination)) {
      $destination = $node->destination;
    }
    if ($node->refresh !== FALSE && $node->refresh !== '' && (int) $node->refresh >= 0) {
      $query = db_insert('node_refresh');
      $query->fields(array(
        'nid' => $node->nid,
        'vid' => $node->vid,
        'seconds' => (int) $node->refresh,
        'destination' => $destination,
      ));
      $query->execute();
    }
  }
}

/**
 * Implements hook_node_update().
 */
function refresh_node_update($node) {
  if (user_access('create refreshes') || user_access('administer refreshes')) {
    $query = db_delete('node_refresh');
    $query->condition('nid', $node->nid);
    $query->condition('vid', $node->vid);
    $query->execute();

    $destination = '';
    if (isset($node->destination)) {
      $destination = $node->destination;
    }
    if ($node->refresh !== FALSE && $node->refresh !== '' && (int) $node->refresh >= 0) {
      $query = db_insert('node_refresh');
      $query->fields(array(
        'nid' => $node->nid,
        'vid' => $node->vid,
        'seconds' => (int) $node->refresh,
        'destination' => $destination,
      ));
      $query->execute();
    }
  }
}

/**
 * Implements hook_form_FORM_ID_alter() for the node type form.
 */
function refresh_form_node_type_form_alter(&$form, &$form_state) {
  $default_value = refresh_get_setting($form['#node_type']->type);

  $form['refresh_fs'] = array(
    '#type' => 'fieldset',
    '#title' => t('Automatic reloading of node'),
    '#weight' => 1,
    '#collapsible' => TRUE,
    '#collapsed' => !$default_value,
    '#group' => 'additional_settings',
    '#attached' => array(
      'js' => array(
        'refresh' => drupal_get_path('module', 'refresh') . '/refresh.js',
      ),
    ),
  );
  $form['refresh_fs']['refresh'] = array(
    '#type' => 'radios',
    '#default_value' => $default_value,
    '#options' => array(
      t('Disabled'),
      t('Enabled'),
    ),
  );
}

/**
 * Gets the refresh setting associated with the given content type.
 */
function refresh_get_setting($type) {
  return variable_get('refresh_' . $type, REFRESH_ENABLED);
}

/**
 * Implements hook_node_type_delete().
 */
function refresh_node_type_delete($info) {
  variable_del('refresh_' . $info->type);
}

/**
 * Implements hook_node_type_update().
 */
function refresh_node_type_update($info) {
  if (!empty($info->old_type) && $info->old_type != $info->type) {
    variable_set('refresh_' . $info->type, refresh_get_setting($info->old_type));
    variable_del('refresh_' . $info->old_type);
  }
}
