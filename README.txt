CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Upgrading
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Refresh module provides a configurable meta refresh when viewing individual nodes.


REQUIREMENTS
------------

This module has no requirements or dependencies to other modules.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

 * You may want to disable the Overlay module, since the functionality this module provides clashes
   with the Refresh module.

 * By default nodes can be refreshed, this functionality can be disabled on a per-content type basis.


UPGRADING
---------

As of the 7.x-1.3 release nodes are refreshed when this is enabled with the content type. This is enabled by default.
The content type setting is available at admin/structure/types/manage/(content type) and acts as global switch
for all nodes of a specific type.


CONFIGURATION
-------------

 * Refresh module settings are available at: admin/config/user-interface/refresh

 * Configuration of individual content (node) types: admin/structure/types/manage/(content type)


MAINTAINERS
-----------

Current maintainers:

 * George Moses (mo6) - https://www.drupal.org/user/32793

 * Tommie Crawford (TommieCrawford) - https://www.drupal.org/user/3414925
