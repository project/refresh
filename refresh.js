(function ($) {

  Drupal.behaviors.refreshFieldsetSummaries = {
    attach: function (context) {
      $('fieldset.refresh-form', context).drupalSetSummary(function (context) {
        var refresh = $('.form-item-refresh input').val();
        return refresh ?
          Drupal.t('Refresh: @refresh seconds', {'@refresh': refresh}) :
          Drupal.t('No refresh');
      });
    }
  };

  Drupal.behaviors.ctRefreshFieldsetSummaries = {
    attach: function (context) {
      $('fieldset#edit-refresh-fs', context).drupalSetSummary(function (context) {

        // Retrieve the value of the selected radio button
        var refresh = $("input[name=refresh]:checked").val();

        if (refresh == 0) {
          return Drupal.t('Disabled');
        }
        else if (refresh == 1) {
          return Drupal.t('Enabled');
        }
      });
    }
  };

})(jQuery);
